using System;
using Manager;
using Unity.Mathematics;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private double fireRate = 0.4f;
        private float fireCounter = 0;
        [SerializeField] private AudioClip enemyFireSound;
        [SerializeField] private float enemyFireSoundVolume = 0.3f;
       
        [SerializeField] private AudioClip enemyExplodeSound;
        [SerializeField] private float enemyExplodeSoundVolume = 0.3f;

        [SerializeField] private Bullet ulti;

        private new Transform transform;
        private bool movingRight = true;
        private bool enrage = false;
        private float movementSpeed = 5f;
        private float ultiShooting;

        private Transform target;
        public void Init(int hp, float speed,PlayerSpaceship target)
        {
            base.Init(hp, speed, defaultBullet);
            this.target = target.transform;
            transform = this.GetComponent<Transform>();
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                if (Hp < 50)
                {
                    enrage = true;
                    fireRate /= 2;
                }
                movementSpeed *= 1.2f;
                movingRight = !movingRight;
                return;
            }
            
            Explode();
        }

        private void Update()
        {
            Move();
        }

        private void Move()
        {
            var mainCamera = Camera.main;
            float minX;
            float maxX;
            minX = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x;
            maxX = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x;

            if (transform.position.x < minX)
            {
                movingRight = false;
            }

            if (transform.position.x > maxX)
            {
                movingRight = true;
            }

            if (!enrage)
            {
                if (!movingRight)
                {
                    var transformPosition = this.transform.position;
                    transformPosition.x += movementSpeed * Time.smoothDeltaTime;
                    transform.position = transformPosition;
                }
                else
                {
                    var transformPosition = this.transform.position;
                    transformPosition.x -= movementSpeed * Time.smoothDeltaTime;
                    transform.position = transformPosition;
                }
            }
            else
            {
                Vector3 delta = target.position - transform.position;
                
                transform.rotation = quaternion.LookRotation(delta,transform.forward);
            }
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(this.gameObject);
            OnExploded?.Invoke();
            AudioSource.PlayClipAtPoint(enemyExplodeSound, Camera.main.transform.position, enemyFireSoundVolume);
        }

        public override void Fire()
        {
            // TODO: Implement this later
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                AudioSource.PlayClipAtPoint(enemyFireSound, Camera.main.transform.position, enemyExplodeSoundVolume);
                var bullet = Instantiate(defaultBullet, gunPosition.position, transform.rotation);
                bullet.Init(-transform.up);
                fireCounter = 0;
                if (enrage)
                {
                    ultiShooting += 1;
                }
            }

            if (ultiShooting > 2)
            {
                AudioSource.PlayClipAtPoint(enemyFireSound, Camera.main.transform.position, enemyExplodeSoundVolume);
                var bullet = Instantiate(ulti, gunPosition.position, transform.rotation);
                bullet.Init(-transform.up);
                ultiShooting = 0;
            }
        }
    }
}