﻿using System;
using System.Collections;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private Button nextButton;
        [SerializeField] private Button endButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private RectTransform nextDialog;
        [SerializeField] private RectTransform endDialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private EnemySpaceship bossSpaceship;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        public static GameManager Instance;

        private ScoreManager scoreManager;

        private PlayerSpaceship playerSpawned;
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");

            scoreManager = ScoreManager.Instance;
            startButton.onClick.AddListener(OnStartButtonClicked);
            nextButton.onClick.AddListener(OnNextButtonClicked);
            endButton.onClick.AddListener(OnEndButtonClicked);
            restartButton.onClick.AddListener(OnRestartButtonClicked);
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }
        
        private void OnNextButtonClicked()
        {
            nextDialog.gameObject.SetActive(false);
            StartCoroutine(NextScene());
        }
        
        private void OnEndButtonClicked()
        {
            endDialog.gameObject.SetActive(false);
            Application.Quit();
        }
        
        private void OnRestartButtonClicked()
        {
            endDialog.gameObject.SetActive(false);
            StartCoroutine(Restart());
        }

        private IEnumerator NextScene()
        {
            yield return SceneManager.LoadSceneAsync("Scenes/Boss");
            SpawnPlayerSpaceship();
            SpawnBossSpaceship();
        }

        private void StartGame()
        {
            scoreManager.Init();
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }
        
        private void SpawnPlayerSpaceship()
        {
            playerSpawned = Instantiate(playerSpaceship);
            playerSpawned.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            playerSpawned.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            StartCoroutine(Restart());
        }

        private void SpawnEnemySpaceship()
        {
            var spaceship = Instantiate(enemySpaceship);
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed,playerSpawned);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
        }

        private void OnEnemySpaceshipExploded()
        {
            scoreManager.AddScore(1);
            if (scoreManager.GetScore() < 2)
            {
                SpawnEnemySpaceship();
                return;
            }
            DestroyRemainingShips();
            nextDialog.gameObject.SetActive(true);
        }
        
        private void SpawnBossSpaceship()
        {
            var spaceship = Instantiate(bossSpaceship);
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed,playerSpawned);
            spaceship.OnExploded += OnBossSpaceshipExploded;
        }

        private void OnBossSpaceshipExploded()
        {
            scoreManager.AddScore(100);
            DestroyRemainingShips();
            endDialog.gameObject.SetActive(true);
        }

        private IEnumerator Restart()
        {
            DestroyRemainingShips();
            yield return SceneManager.LoadSceneAsync("Scenes/Game");
            scoreManager.ResetScore();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void DestroyRemainingShips()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }

            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }
        }

        private void Start()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            
            if (Instance != this)
            {
                Destroy(this.gameObject);
            }
            
            DontDestroyOnLoad(Instance);
        }
    }
}
