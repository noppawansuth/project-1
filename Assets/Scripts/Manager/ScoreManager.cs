﻿using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;

        private int playerScore;
        public static ScoreManager Instance;
        
        public void Init()
        {
            GameManager.Instance.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore();
        }

        private void SetScore()
        {
            scoreText.text = $"Score : {playerScore}";
        }

        public int GetScore()
        {
            return playerScore;
        }

        public void AddScore(int score)
        {
            playerScore += score;
            SetScore();
        }

        public void ResetScore()
        {
            playerScore = 0;
        }
        
        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");

            if (Instance == null)
            {
                Instance = this;
            }

            if (Instance != this)
            {
                Destroy(this.gameObject);
            }
            
            DontDestroyOnLoad(Instance);
        }
        
        private void OnRestarted()
        {
            //finalScoreText.text = $"High Score : {playerScore}";
            GameManager.Instance.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore();
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }
}


